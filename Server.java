import java.util.*;	//contain classes for Array,Linkedlist,Coparator,stack,scanner,etc
import java.io.*;	//contain java classes for input and output
 
public class Main {	//this is the main class
    
 
    class Pair {	//This is the nested class
        int first;
        int second;
 
        public Pair(int first, int second) {	//constractor, show which server is connected to another
            this.first = first;	//first server
            this.second = second;	//second server connected to the first one to form a pair
        }
    }
 
    public void run() throws Exception {
        
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();	//read number of servers from the user
        int m = in.nextInt();	//read number of pair of the server connected

        Pair connections[] = new Pair[m];
        for (int i = 0; i < m; i++) {		//read pair connection which depends on number of pair (m)
            connections[i] = new Pair(in.nextInt() - 1, in.nextInt() - 1);
        }
        int[] dp = new int[1<<n];
        Arrays.fill(dp, Integer.MAX_VALUE);
        dp[0] = 0;
        for (int bits = 0; bits < n; bits++) {
            for (int mask = 0; mask < (1 << n); mask++) if (Integer.bitCount(mask) == bits) {
                int add = 0;
                for (Pair connection : connections) {
                    boolean first = (mask & (1<<connection.first)) > 0;
                    boolean second = (mask & (1<<connection.second)) > 0;
                    if (first != second) {
                        add++;
                    }
                }
                for (int i = 0; i < n; i++) if ((mask & (1<<i)) == 0) {
                    dp[mask | (1<<i)] = Math.min(dp[mask | (1<<i)], dp[mask] + add);
                }
            }
        }
        out.println(dp[(1<<n) - 1]);
        out.close();
        in.close();
    }
 
    public static void main(String[] args) throws Exception {
        new Main().run();
    }
}